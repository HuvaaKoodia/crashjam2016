﻿using UnityEngine;
using System.Collections;

public class ParticleBehaviour : MonoBehaviour {
    
    public float minSpeed = 1, maxSpeed = 5;

    private float speedX, speedY;
    public Transform initBound, endBound;

	void Start ()
    {
        speedX = Random.Range(minSpeed, maxSpeed);
        speedY = Random.Range(minSpeed, maxSpeed);

        initBound = GameObject.Find("Init_Bound").gameObject.GetComponent<Transform>();
        endBound = GameObject.Find("End_Bound").gameObject.GetComponent<Transform>();
    }
	
	void Update () {

        transform.position += new Vector3(speedX * Time.deltaTime, - speedY * Time.deltaTime);

        Debug.Log("pos x " + transform.position.x);
        Debug.Log("endbound pos x " + endBound.position.x);
        Debug.Log("pos y " + transform.position.y);
        Debug.Log("endbound pos y " + endBound.position.y);
        Debug.Log("initbound pos x " + initBound.position.x);
        Debug.Log("initbound pos y " + initBound.position.y);

        if (transform.position.x > endBound.position.x || transform.position.y < endBound.position.y // careful, the y is negative here, because the scene is centered
            || transform.position.x < initBound.position.x || transform.position.y > initBound.position.y) // careful, the y is positive here, because the scene is centered
        {
            Destroy(gameObject); Debug.Log("destroying!");
        }
    }
}
