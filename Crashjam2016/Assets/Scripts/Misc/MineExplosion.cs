﻿using UnityEngine;
using System.Collections;

public class MineExplosion : MonoBehaviour
{
    public ExplosionOnHit ExplosionOnHit;

    // Use this for initialization
    void Start () {
        StartCoroutine(mineExplosion());
	}
	
	// Update is called once per frame
	void Update () {


    }
    private IEnumerator mineExplosion()
    {
        yield return new WaitForSeconds(1);
        ExplosionOnHit.PlayExplosion();
        Debug.Log("Gone!");
        Destroy(gameObject);
    }
}
