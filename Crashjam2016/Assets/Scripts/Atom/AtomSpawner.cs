﻿using UnityEngine;
using System.Collections;

public class AtomSpawner : MonoBehaviour
{
    public float StartPositionOffset = 1f;
    public float StartVelocityMin = 2f, StartVelocityMax = 3f;
    public AnimationCurve SpawnDelayCurve;
    public float SpawnDelayCurveDuration = 60f;
    public float SpawnDelayCurveMultiplier = 1f;

    void Start ()
    {
        StartCoroutine(Spawner());
	}

    IEnumerator Spawner()
    {
        float startTime = Time.time;
        while (true)
        {
            int spawnDirection = 0;
            var position = WorldController.I.GetRandomStartPosition(out spawnDirection, StartPositionOffset);
            var target = WorldController.I.GetRandomTargetPosition(spawnDirection);
            AtomGenerator.I.GenerateAtom(position, position.ToNorm(target) * Helpers.Rand(StartVelocityMin, StartVelocityMax), Helpers.Rand(1, 4), spawnDirection== 3 ? -Vector2.right : Vector2.right);

            float currentTime = (Time.time - startTime) / SpawnDelayCurveDuration;
            float spawnDelay = SpawnDelayCurve.Evaluate(currentTime) * SpawnDelayCurveMultiplier;
            //Debug.Log("Spawn delay: " + spawnDelay);
            yield return new WaitForSeconds(spawnDelay);

        }
    }
}