using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using System;

public class MusicController : MonoBehaviour
{
    public static MusicController I;

    private int trackIndex = -1;
    public AudioClip AmbientTrack;
    public AudioClip[] MusicTracks;
    public AudioSource MusicSource, AmbientSource;

    public AudioMixer Mixer;
    private AudioMixerSnapshot MusicUp, MusicDown, AmbientUp, AmbientDown;

    private void Awake()
    {
        if (I)
        {
            Destroy(this);
            return;
        }

        DontDestroyOnLoad(gameObject);
        I = this;

        MusicUp = Mixer.FindSnapshot("MusicUp");
        MusicDown = Mixer.FindSnapshot("MusicDown");
        AmbientUp = Mixer.FindSnapshot("AmbientUp");
        AmbientDown = Mixer.FindSnapshot("AmbientDown");
    }

    public void StartAmbientTrack()
    {
        trackIndex = -1;
        AmbientVolumeInc(1f);
        StartClip(AmbientTrack, AmbientSource);
    }

    public void StartMusicTrack(int index)
    {
        if (trackIndex == index) return;
        trackIndex = index;
        MusicVolumeInc(1f);
        StartClip(MusicTracks[index], MusicSource);
    }

    void StartClip(AudioClip clip, AudioSource source)
    {
        source.loop = true;
        source.clip = clip;
        source.Play();
    }

    public void MusicVolumeInc(float time)
    {
        MusicUp.TransitionTo(time);
    }

    public void MusicVolumeDec(float time)
    {
        MusicDown.TransitionTo(time);
    }

    public void AmbientVolumeInc(float time)
    {
        AmbientUp.TransitionTo(time);
    }

    public void AmbientVolumeDec(float time)
    {
        MusicUp.TransitionTo(time);
    }

    public void SetMusicVolumeFull()
    {
        MusicUp.TransitionTo(0);
    }
}
