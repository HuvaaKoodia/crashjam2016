﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

/// <summary>
/// This whole class is a dirty hack... Beware!
/// </summary>
public class TextDisplayer : MonoBehaviour
{
    public GameObject textBox;
    public MenuBehaviour menu;
    public Text theText;
    public TextAsset textFile;
    public string[] textLines;
    public AudioPlayer TextScrollAudio;

    bool exception = false;
    private bool isTyping = false;
    public bool isActive = true;
    public bool cancelTyping = false;
    public bool firstClick = true;

    public int state = 0;
    public float value = 0;
    public float stateDelay;
    public float stateTimer = 0;
    public int currentLine;
    public int endAtLine;
    public float typeSpeed;

    void Start()
    {
        ReloadScript(textFile, 0, 3);

    }

    void Update()
    {

        if (state != 3 && Input.GetMouseButtonDown(0))
        {
            //skip the whole shebang!
            state = 2;
        }

        if (state == 100)
        {
            return;
        }

        stateDelay += Time.deltaTime;

        if (state == 0)
        {
            value += Time.deltaTime;
            if (value > 0.4f)
            {
                theText.text = "_";
                if (value > 0.8f)
                    value = 0;
            }
            else
            {
                theText.text = " ";
            }

            if (stateDelay > 2.8f)
                state++;
        }

        else if (state == 1)
        {
            stateDelay += Time.deltaTime;
            if (isActive)
            {
                if (Input.GetMouseButtonDown(0) || firstClick)
                {
                    if (!isTyping)
                    {
                        if (firstClick)
                        {
                            firstClick = false;
                        }
                        else
                        {
                            currentLine++;
                        }

                        if (currentLine > endAtLine)
                        {
                            DisableTextBox();
                        }
                        else
                        {
                            textScroll = TextScroll(textLines[currentLine]);
                            StartCoroutine(textScroll);
                        }
                    }
                    else if (isTyping && !cancelTyping)
                    {
                        cancelTyping = true;
                    }
                }
            }
            if (stateDelay > 10)
            {
                if (!exception)
                {
                    state = 2;
                    firstClick = true;
                    currentLine++;
                    exception = true;
                }
            }

        }

        else if (state == 2)
        {
            if (menu != null)
                menu.Activate();

            if (textScroll != null) StopCoroutine(textScroll);

            StartCoroutine(TextScroll(textLines[textLines.Length - 1]));

            state = 3;
        }

    }

    IEnumerator textScroll;

    private IEnumerator TextScroll(string lineOfText)
    {
        int letter = 0; // int for the number of letters
        theText.text = "";

        isTyping = true;
        cancelTyping = false;

        while (isTyping && !cancelTyping && (letter < lineOfText.Length - 1))
        {
            if (theText.text.EndsWith("_"))
            {
                Debug.Log("wow");
            }

            TextScrollAudio.Play2DSound();
            theText.text += lineOfText[letter]; // adds one letter to the textBox
            // theTextComplete = theText.text + "_";
            letter += 1; // adds 1 to letter variable
            yield return new WaitForSeconds(typeSpeed); // waits for an amount of time
        }
        // the loop ends if the line has reached its end

        // theText.text = lineOfText + "_";
        isTyping = false;
        cancelTyping = false;
        textScroll = null;
    }

    public void DisableTextBox(bool endDialogue = true) // deactivates all text output, enables player movement
    {
        textBox.SetActive(false);
        isActive = false;
    }

    public void ReloadScript(TextAsset newText, int getCurrentLine, int endLine)
    {
        currentLine = getCurrentLine; // current line in text
        endAtLine = endLine; // current line you want to finish the text

        textFile = newText;

        if (textFile != null)
        {
            textLines = (textFile.text.Split('\n')); // splits the text every endLine
        }

        if (newText != null)
        {
            textLines = new string[1]; // checks if the next letter after current is an endLine
            textLines = (newText.text.Split('\n')); // ands splits it if it is
        }
    }
}