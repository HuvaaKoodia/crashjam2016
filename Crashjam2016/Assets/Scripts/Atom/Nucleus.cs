﻿using UnityEngine;
using System.Collections;

public class Nucleus : MonoBehaviour
{
    public GravityWell gravityWell;
    public new Rigidbody2D rigidbody;
    public InteractionSystem InteractionSystem;
    public LayerMask FusionMask;
    public ExplosionOnHit FusionParticleExplosion;
    public GrowOverTime GrowOverTime;

    public float fusionCount = 3;
    public float fusionVelocity = 2.5f;

    public float[] fusionMass, fusionGravityWellForce, fusionSize;

    private Vector3 scale;
    int currentFusionCount = 0;
    

    public void Init(Vector3 velocity)
    {
        rigidbody.mass = fusionMass[currentFusionCount];
        gravityWell.force = fusionGravityWellForce[currentFusionCount];
        transform.localScale = Vector3.one * fusionSize[currentFusionCount];

        rigidbody.AddForce(velocity * rigidbody.mass, ForceMode2D.Impulse);
        InteractionSystem.OnCollisionReceived += OnCollisionReceived;
    }

    private bool consumedInFusion = false;//hack!

    private void OnCollisionReceived(InteractionSystem target)
    {
        if (consumedInFusion) return;

        //fusion particle hit
        if ((target.Mask & FusionMask.value) == target.Mask)
        {
            var targetNucleus = target.GetComponent<Nucleus>();//HACK!

            //collide with it
            if (target.Rigidbody.velocity.magnitude < fusionVelocity)
            {//explosion between the particles
                FusionParticleExplosion.PlayExplosion(Vector3.Lerp(rigidbody.position, target.Rigidbody.position, 0.5f));
                return;
            }
            else
            {//fuse with it
                if (rigidbody.mass >= target.Rigidbody.mass)
                {
                    targetNucleus.consumedInFusion = true;
                    target.Die();

                    currentFusionCount++;

                    if (currentFusionCount > fusionCount) return;

                    if (currentFusionCount == fusionCount)
                    {//blackhole time!
                        StartCoroutine(CreateBlackholeCoroutine());
                        return;
                    }

                    //change mass and gravity well force
                    rigidbody.mass = fusionMass[currentFusionCount];
                    gravityWell.distanceAffectsForce = true;
                    gravityWell.force = fusionGravityWellForce[currentFusionCount];
                    
                    //change size and velocity
                    GrowOverTime.Grow(Vector3.one * fusionSize[currentFusionCount], 1f);
                    float speed = rigidbody.velocity.magnitude * 0.2f;
                    rigidbody.velocity = rigidbody.position.ToNorm(WorldController.I.GetRandomCenterPosition()) * speed;
                }
            }
        }
    }

    private IEnumerator CreateBlackholeCoroutine()
    {
        GrowOverTime.Grow(Vector3.zero, 1f);
        yield return new WaitForSeconds(1f);
        AtomGenerator.I.GenerateBlackhole(transform.position);
        Destroy(gameObject);
    }
}
