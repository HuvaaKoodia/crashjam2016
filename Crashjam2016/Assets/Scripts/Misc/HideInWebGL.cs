﻿using UnityEngine;

public class HideInWebGL : MonoBehaviour 
{
    #region variables
    #endregion
    #region initialization
#if UNITY_WEBGL
    private void Start () 
	{
        gameObject.SetActive(false);
    }
#endif
#endregion
    #region update logic
    #endregion
    #region public interface
    #endregion
    #region private interface
    #endregion
    #region events
    #endregion
}
