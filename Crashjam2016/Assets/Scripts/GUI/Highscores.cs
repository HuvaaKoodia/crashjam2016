﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Highscores : MonoBehaviour {
    public Text text;


    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	public void DisplayHighScore (float highScore) {
        text.text = "Highscore: " + highScore.ToString("0");
    }
}
