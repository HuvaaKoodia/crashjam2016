﻿using UnityEngine;
using System.Collections;

public class VFXController : MonoBehaviour {

    public AnimationCurve distortionOverTime;
    public AnimationCurve scaleOverTime;

    public Renderer rend;

    public float distortion;
    public float scale;

    public float destructionTime = 1.0f;
   
    public float timer = 0;
	// Use this for initialization
	void Start () 
    {
        rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () 
    {

        timer += Time.deltaTime; 

        scale = scaleOverTime.Evaluate(timer); 
        transform.localScale = new Vector3(scale,scale,scale);

        //do not use values over 1 in the curve edeitor
        distortion = 128*distortionOverTime.Evaluate(timer);
        rend.material.SetFloat("_BumpAmt", distortion);

        if (timer >= destructionTime)
        Destroy(gameObject);
	}
}
