﻿using UnityEngine;
using System.Collections;

public class TrailEffect : MonoBehaviour {

    public bool trailDeath = false;
    TrailRenderer trail;
    float timer = 1;

	// Use this for initialization
	void Start () 
    {
        trail = GetComponent<TrailRenderer>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if(trailDeath)
        {
            
            timer -= Time.deltaTime;
            trail.startWidth = trail.startWidth * timer;

            if (timer <= 0)
                Destroy(gameObject);
        }
	}

    public void DetachTrail()
    {
        trailDeath = true;
        transform.parent = null;
    }
}
