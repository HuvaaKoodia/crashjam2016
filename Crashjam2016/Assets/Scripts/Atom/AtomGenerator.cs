﻿using UnityEngine;
using System.Collections;

public class AtomGenerator : MonoBehaviour
{
    public static AtomGenerator I;

    public BlackHole BlackHolePrefab;
    public Nucleus NucleusPrefab;
    public Electron ElectronPrefab;
    public bool GenerateOnStart = true, GenerateBlackholeOnStart = false;

    void Awake()
    {
        I = this;
    }

    void Start()
    {
        if (GenerateOnStart) GenerateAtom(Vector2.zero, Vector2.zero, 3, Vector2.right);

        if (GenerateBlackholeOnStart) GenerateBlackhole(Vector2.right * 6);
    }

    public void GenerateAtom(Vector2 position, Vector2 velocity, int electronAmount, Vector2 electronSpawnDirection)
    {
        var nucleus = Instantiate(NucleusPrefab, position, Quaternion.identity) as Nucleus;
        nucleus.Init(velocity);
        float gravityWellForce = nucleus.gravityWell.force;

        int electronDistance = 1;

        for (int i = 0; i < electronAmount; i++)
        {
            float distance = electronDistance * (i + 1);
            var electron = Instantiate(ElectronPrefab, position + electronSpawnDirection * distance, Quaternion.identity) as Electron;
            electron.Init(position, velocity, i % 2 == 0 ? Vector2.up : Vector2.down, gravityWellForce, distance);
        }
    }

    public void GenerateBlackhole(Vector2 position)
    {
        var blackhole = Instantiate(BlackHolePrefab, position, Quaternion.identity) as BlackHole;
    }
}
