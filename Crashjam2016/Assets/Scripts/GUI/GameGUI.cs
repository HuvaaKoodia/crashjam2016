﻿using UnityEngine;
using System.Collections.Generic;

public class GameGUI : MonoBehaviour 
{
    #region variables
    public GameObject gameoverPanel;
    #endregion
    #region initialization
    private void Start () 
	{
        if (WorldController.I) WorldController.I.OnGameoverEvent += OnGameOver;

        gameoverPanel.SetActive(false);
    }
#endregion
#region update logic
    #endregion
    #region public interface
    #endregion
    #region private interface
    #endregion
    #region events
    private void OnGameOver()
    {
        gameoverPanel.SetActive(true);
    }

    #endregion
}
