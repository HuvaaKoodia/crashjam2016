﻿using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour {

    public GameObject targetCamera;

	// Use this for initialization
    void Start()
    {
        targetCamera = GameObject.FindWithTag("MainCamera");
    }
 
    void Update()
    {
        transform.LookAt(transform.position + targetCamera.transform.rotation * Vector3.forward,
            targetCamera.transform.rotation * Vector3.up);
    }
}
