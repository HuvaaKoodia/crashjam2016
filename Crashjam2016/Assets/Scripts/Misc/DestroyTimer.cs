﻿using UnityEngine;
using System.Collections;

public class DestroyTimer : MonoBehaviour {

    public float destructionTime = 1.0f;
   
    public float timer = 0;
	// Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame
	void Update () 
    {

        timer += Time.deltaTime; 

        if (timer >= destructionTime)
        Destroy(gameObject);
	}
}
