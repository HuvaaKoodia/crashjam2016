﻿using UnityEngine;
using System.Collections;

public class Electron : MonoBehaviour
{
    public Collider2D Collider;
    public Rigidbody2D Rigidbody;

    public float StartDistance = 2;

    public void Init(Vector2 gravityWellCenter, Vector2 startVelocity, Vector2 startForceDirection, float gravityWellForce, float distance)
    {
        float startForce = Mathf.Sqrt(gravityWellForce * distance) ;
        Rigidbody.AddForce(startVelocity + startForceDirection.normalized * startForce, ForceMode2D.Impulse);
    }
}
