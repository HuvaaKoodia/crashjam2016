﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public static Player I;
    public float onExplosionInputDelay = 1f;

    public Movement movement;
    public InteractionSystem interactionSystem;
    public ExplosionOnHit explosionOnHit;
    public Rigidbody2D rigidbody;
    public new ParticleSystem particleSystem;

    private IEnumerator inputDelayEnumerator;
    
    void Awake()
    {
        I = this;
        interactionSystem.OnExplosionReceivedEvent += OnExplosionReceived;
    }
    
    void Update()
    {
        if (inTrigger)
        {
            inTrigger = false;
        }
        else particleSystem.emissionRate = 30.0f;
    }

    void OnExplosionReceived()
    {
        if (inputDelayEnumerator != null) StopCoroutine(inputDelayEnumerator);
        inputDelayEnumerator = InputDelayCoroutine();
        StartCoroutine(inputDelayEnumerator);
    }

    private IEnumerator InputDelayCoroutine()
    {
        movement.enabled = false;
        yield return new WaitForSeconds(onExplosionInputDelay);
        movement.enabled = true;
        inputDelayEnumerator = null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Deadly") interactionSystem.Die();
    }

    public void OnTriggerStay2D(Collider2D collider)
    {
        var targetGravityWell = collider.GetComponent<GravityWell>();
        if (targetGravityWell == null) return;

        var circleCollider = (CircleCollider2D)collider;
        float distance = Vector2.Distance(circleCollider.transform.position, transform.position);
        float reverseDistance = 1 - Mathf.Clamp01(distance / (circleCollider.radius * circleCollider.transform.lossyScale.x));
        float pointvalue = ((targetGravityWell.force * 0.1f) * reverseDistance);
        ScoreController.I.AddScore(pointvalue);

        inTrigger = true;
        particleSystem.emissionRate = Mathf.Lerp(30f, 500f, reverseDistance);
        Debug.Log(reverseDistance);
    }

    bool inTrigger = false;
}
