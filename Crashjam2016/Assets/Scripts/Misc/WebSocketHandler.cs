﻿using UnityEngine;
using System.Collections;
using WebSocketSharp;
using System.Text.RegularExpressions;

public class WebSocketHandler : MonoBehaviour {


    public AudioSource WarningAudio;

    public static WebSocketHandler I;

    bool pendingAtomSpawn = false;
    bool pendingBlackHoleSpawn = false;
    bool pendingGrantPacman = false;
    bool pendingConnect = false;

    bool pendingKeepAlive = false;
    WebSocket ws = null;

	// Use this for initialization
	void Start () {
        StartConnection();
	}

    void Awake()
    {
        I = this;
    }

    public void submitHighscore(float score)
    {
        Debug.Log("submitting score to server.");
        string scoreJson = serializeMsg("submitHighscore\",\"" + score.ToString());
        Debug.Log(scoreJson);
        ws.Send(scoreJson);
    }

    private void StartConnection()
    {
        ws = initWebSocket();
        pendingConnect = true;
        pendingKeepAlive = true;
    }

    private void QueueUpKeepAlive()
    {
        StartCoroutine(KeepAliveTimer());
    }

    private IEnumerator KeepAliveTimer()
    {
        yield return new WaitForSeconds(3);
        pendingKeepAlive = true;
    }

    private string getStringFromMsg(string jsonMsg, int wantedMatch)
    {
        string[] matches = new string[] 
        {
            "empty",
            "empty"
        };
        int i = 0;
        foreach (Match match in Regex.Matches(jsonMsg, "\"([^\"]*)\""))
        {
            matches[i] = match.ToString();
            i++;
        }
        if (matches[wantedMatch] != "empty")
        {
            string eventName = matches[wantedMatch].Replace("\"", "");
            return eventName;
        }
        else
        {
            return "no match";
        }
    }

    private void OnOpenHandler (object sender, System.EventArgs e) {
        Debug.Log("connection opened.");
    }

    private void OnCloseHandler(object sender, WebSocketSharp.CloseEventArgs e)
    {
        Debug.Log("connection closing with code: " + e.Code);
        StartConnection();
    }

    private void OnMessageHandler(object sender, WebSocketSharp.MessageEventArgs e)
    {
        string eventName = getStringFromMsg(e.Data, 0);
        string eventContent = getStringFromMsg(e.Data, 1);
        Debug.Log("Received event: " + eventName);
        if (eventContent != "no match")
        {
            Debug.Log("Event has content: " + eventContent);
        }

        if (eventName == "spawnAtom")
        {
            Debug.Log("spawning atom.");
            pendingAtomSpawn = true;
        }
        else if (eventName == "spawnBlackHole")
        {
            Debug.Log("spawning black hole.");
            pendingBlackHoleSpawn = true;
        }
        else if (eventName == "grantPacman")
        {
            Debug.Log("making player pacman.");
            // how to check that player still exists?
            pendingGrantPacman = true;
        }
    }

    private void SpawnAtom()
    {
        PlayWarningSound();

        Vector3 extents = WorldController.I.LevelBounds.extents;
        Vector2 randPos = new Vector2();
        Vector2 randVel = new Vector2();
        randPos.x = Helpers.Rand(-extents.x+3.0f, extents.x-3.0f);
        randPos.y = Helpers.Rand(-extents.y+3.0f, extents.y-3.0f);
        randVel.x = Helpers.Rand(-extents.x*0.1f, extents.x*0.1f);
        randVel.y = Helpers.Rand(-extents.y*0.1f, extents.y*0.1f);
        AtomGenerator.I.GenerateAtom(randPos, randPos, 3, Vector2.right);
    }

    private void GrantPacmanToPlayer()
    {
        //Player.I.GrantPacman();
    }

    private void SpawnBlackHole()
    {
        PlayWarningSound();

        Vector3 extents = WorldController.I.LevelBounds.extents;
        Vector3 randPos = WorldController.I.LevelBounds.center;
        randPos.x = Helpers.Rand(-extents.x, extents.x);
        randPos.y = Helpers.Rand(-extents.y, extents.y);
        Debug.Log(randPos);
        AtomGenerator.I.GenerateBlackhole(randPos);
    }

    private WebSocket initWebSocket()
    {
        var ws = new WebSocket("ws://46.101.187.53:3000/socket.io/?EIO=2&transport=websocket");
        ws.OnError += (sender, e) =>
        {
            Debug.Log(e.Message);
            Debug.Log(e.Exception);
        };
        ws.OnOpen += OnOpenHandler;
        ws.OnClose += OnCloseHandler;
        ws.OnMessage += OnMessageHandler;
        return ws;   
    }

    private string serializeMsg (string msg) {
        return "42[\"" + msg + "\"]";
    }

    public IEnumerator keepWsAlive(WebSocket ws)
    {
        using (ws)
        {
            Debug.Log("connecting");
            ws.Connect();
            //string initMsg = "42[\"IAmGame\", \"something\"]";
            string initMsg = serializeMsg("IAmGame");
            ws.Send(initMsg);

            while (true)
            {
                yield return null;
            }
        }
    }

	// Update is called once per frame
	void Update () {
        if (pendingConnect)
        {
            StartCoroutine(keepWsAlive(ws));
            pendingConnect = false;
        }
        if (pendingKeepAlive)
        {
            ws.Send(serializeMsg("ImStillHereDammit"));
            //Debug.Log("Keepalive sent.");
            pendingKeepAlive = false;
            QueueUpKeepAlive();
        }

        if (pendingAtomSpawn)
        {
            SpawnAtom();
            pendingAtomSpawn = false;
        }
        if (pendingBlackHoleSpawn)
        {
            SpawnBlackHole();
            pendingBlackHoleSpawn = false;
        }
        if (pendingGrantPacman)
        {
            if (GameObject.Find("Player") != null)
            {
                GrantPacmanToPlayer();
            }
            pendingGrantPacman = false;
        }
    }

    private void PlayWarningSound()
    {
        if (!WarningAudio.isPlaying) WarningAudio.Play();
    }
}
