﻿using UnityEngine;
using System.Collections;

public class ActivateOnAwake : MonoBehaviour 
{
    public bool Active = false;
    public GameObject Target;

	void Awake () 
    {
        if (!Target) Target = gameObject;
        Target.SetActive(Active);
	}
}
