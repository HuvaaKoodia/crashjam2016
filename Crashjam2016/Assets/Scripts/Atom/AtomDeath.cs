﻿using UnityEngine;
using System.Collections;

public class AtomDeath : MonoBehaviour
{
    public GameObject deathEffect;
    public InteractionSystem InteractionSystem;
    public AudioPlayer DeathAudio;

    public TrailEffect trail;

    void Awake ()
    {
        InteractionSystem.OnDeathEvent += OnDeath;
        InteractionSystem.OnDestroyEvent += OnAtomDestroy;
    }	

    private void OnDeath()
    {
        if (trail != null)
            trail.DetachTrail();
        
        if (deathEffect != null)
            Instantiate(deathEffect, transform.position, Quaternion.identity);
        GameObject.Destroy(gameObject); 
		if (DeathAudio) DeathAudio.Play2DSound();		
    }

    private void OnAtomDestroy()
    {
        if (trail != null)
            trail.DetachTrail();

        GameObject.Destroy(gameObject);
		
    }
}
