﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuBehaviour : MonoBehaviour
{
    public CanvasGroup CanvasGroup;

    public AudioPlayer ButtonAudio;
    public AudioPlayer StartupAudio;
    public AudioPlayer PlayAudio;
    public AudioPlayer QuitAudio;

    void Start()
    {
        AudioController.I.StopAllAudio();
        MusicController.I.StartAmbientTrack();
        gameObject.SetActive(false);
    }

    public void Activate()
    {
        gameObject.SetActive(true);
        //StartupAudio.Play2DSound();
    }

    public void PlayPressed()
    {
        //PlayAudio.Play2DSound();
        ButtonAudio.Play2DSound();
        StartCoroutine(DelayCoroutine(2f, () => SceneManager.LoadScene("Game")));
    }
    
    public void CreditsPressed()
    {
        SceneManager.LoadScene("Credits");
    }

    public void QuitPressed()
    {
        //QuitAudio.Play2DSound();
        ButtonAudio.Play2DSound();
        StartCoroutine(DelayCoroutine(2f, () => Application.Quit()));
    }

    public IEnumerator DelayCoroutine(float delay, System.Action action)
    {
        CanvasGroup.interactable = false;
        yield return new WaitForSeconds(delay);
        action();
    }
}
