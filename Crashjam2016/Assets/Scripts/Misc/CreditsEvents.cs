﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CreditsEvents : MonoBehaviour
{
    public GameObject creditsImage;
    public GameObject backButton;

    public bool isJKButtonActive = true;

    public float buttonTimer;

    public void OnBackButtonClick()
    {
        SceneManager.LoadScene("MenuScene");
    }


    void Start()
    {
        MusicController.I.AmbientVolumeDec(0);
    }

}
