﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class LensFocusEffect : MonoBehaviour
{
    public static bool reducedDelay = false;

    public Text text;
    public BlurOptimized blur;
    public Image blackCover;
    public AudioPlayer zoomAudio, focusedAudio;

    private float blurDuration;

    IEnumerator Start()
    {
        float delay = 2f;
        if (reducedDelay)
            delay = 0.5f;

        text.text = "Focusing Lens ... ";
        Time.timeScale = 0.0f;

        yield return new WaitForSecondsRealtime(delay);

        blurDuration = 2f;
        var blurTimer = blurDuration;
        blur.blurIterations = 6;

        while (blurTimer > 0f)
        {
            float percent = (blurTimer) / (blurDuration);
            percent = Mathf.Clamp01(percent);
            blackCover.color = new Color(0, 0, 0, percent);
            blurTimer -= Time.unscaledDeltaTime;

            if (blurTimer < 0.4f)
            {
                SetBlur(1);
                Time.timeScale = 0.4f;
            }
            else if (blurTimer < 0.8f)
            {
                SetBlur(2);
                Time.timeScale = 0.3f;
            }
            else if (blurTimer < 1.2f)
            {
                SetBlur(3);
                Time.timeScale = 0.2f;

            }
            else if (blurTimer < 1.6f)
            {
                SetBlur(4);
                Time.timeScale = 0.1f;
            }
            else if (blurTimer < 2.0f)
            {
                SetBlur(5);
                Time.timeScale = 0.1f;
            }

            yield return null;
        }

        Time.timeScale = 1.0f;
        blur.enabled = false;
        blackCover.enabled = false;
        SetBlur(0);

        text.text = "FOCUSED.";

        yield return new WaitForSeconds(1f);
        text.enabled = false;
    }

    private void SetBlur(int iterations)
    {
        if (blur.blurIterations == iterations) return;
        blur.blurIterations = iterations;

        if (blur.blurIterations == 0)
            focusedAudio.Play2DSound();
        else
            zoomAudio.Play2DSound();
        text.text = "Focusing Lens ... " + blur.blurIterations;
    }
}
