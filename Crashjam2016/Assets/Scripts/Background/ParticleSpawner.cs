﻿using UnityEngine;
using System.Collections;

public class ParticleSpawner : MonoBehaviour {
    
    public GameObject particle;
    public Transform initBound, endBound;
    public Vector3 spawnPosition;
    public float timer;

    void Start () {
        initBound = transform.Find("Init_Bound").gameObject.GetComponent<Transform>();
        endBound = transform.Find("End_Bound").gameObject.GetComponent<Transform>();
    }
	
	void Update () {
        timer += Time.deltaTime;
        if (timer > 2.0f)
        {
            spawnPosition = new Vector3(Random.Range(initBound.position.x, endBound.position.x), initBound.position.y, 0);
            Instantiate(particle, spawnPosition, Quaternion.identity);
            timer = 0;
        }
	}
}
