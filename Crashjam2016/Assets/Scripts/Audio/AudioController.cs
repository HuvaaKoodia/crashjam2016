﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour
{
#region vars
	public static AudioController I;

    public int ChannelAmount = 1;
    public int AudioSourceAmount = 20;
    public AudioSource AudioSourcePrefab;

    private AudioSource[,] audioSources;
    private int[] currentSourceIndex;
#endregion
#region init
	private void Awake()
	{
        if (I != null)
        {
            DestroyImmediate(gameObject);
            return;
        }

		I = this;
        DontDestroyOnLoad(gameObject);

        audioSources = new AudioSource[ChannelAmount, AudioSourceAmount];
        currentSourceIndex = new int[ChannelAmount];

        for (int c = 0; c < ChannelAmount; c++)
        {
            for (int a = 0; a < AudioSourceAmount; a++)
            {
                audioSources[c,a] = Instantiate(AudioSourcePrefab) as AudioSource;
                DontDestroyOnLoad(audioSources[c, a].gameObject);
                audioSources[c,a].transform.parent = transform;
            }
        }

        if (AudioSourcePrefab.transform.parent == transform) AudioSourcePrefab.gameObject.SetActive(false);
    }
    #endregion
    #region logic
    #endregion
    #region public interface
    public void StopAllAudio()
    {
        foreach (var audio in audioSources)
        {
            audio.Stop();
        }
    }

    public void PlayAudio(AudioClip clip, AudioMixerGroup mixerGroup, int channel = 0)
    {
        var source = audioSources[channel, currentSourceIndex[channel]];
        currentSourceIndex[channel]++;
        if (currentSourceIndex[channel] == AudioSourceAmount) currentSourceIndex[channel] = 0;

        source.spatialBlend = 0f;
        source.outputAudioMixerGroup = mixerGroup;
        source.PlayOneShot(clip);
    }

    public void PlayAudio(Vector3 position, AudioClip clip, AudioMixerGroup mixerGroup, int channel = 0)
    {
        var source = audioSources[channel, currentSourceIndex[channel]];
        currentSourceIndex[channel]++;
        if (currentSourceIndex[channel] == AudioSourceAmount) currentSourceIndex[channel] = 0;

        source.spatialBlend = 1f;
        source.outputAudioMixerGroup = mixerGroup;
        source.transform.position = position;
        source.PlayOneShot(clip);
    }
#endregion
#region private interface
#endregion
#region events
#endregion
}