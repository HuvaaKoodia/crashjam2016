﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldController : MonoBehaviour
{
    #region vars
    public static WorldController I;

    public Player Player;
    public BoxCollider2D LevelCollider;
    public Bounds LevelBounds { get { return LevelCollider.bounds; } }

    public System.Action OnGameoverEvent;

    private Vector3[] points = new Vector3[8];
    #endregion
    #region init
    void Awake()
    {
        I = this;

        //move camera to show the whole level
        float levelHeight = LevelBounds.extents.y;
        Camera camera = Camera.main;

        float angle = camera.fieldOfView * 0.5f;
        float optimalDistance = levelHeight / Mathf.Tan(angle * Mathf.Deg2Rad);

        camera.transform.position = new Vector3(0, 0, -optimalDistance);
        LevelCollider.size = new Vector2(camera.aspect * levelHeight * 2, levelHeight * 2);

        if (Player) Player.interactionSystem.OnDeathEvent += OnPlayerDeath;
    }

    void Start()
    {
        AudioController.I.StopAllAudio();
        MusicController.I.StartMusicTrack(0);
    }

    #endregion
    #region logic
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartLevel();
        }

        if (Gameover)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                GotoMenu();
            }
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.B))
        {
            var mouseScreenPosition = Input.mousePosition;
            mouseScreenPosition.z = -Camera.main.transform.position.z;
            var mouseWorldPosition = Camera.main.ScreenToWorldPoint(mouseScreenPosition);
            mouseWorldPosition.z = 0;
            AtomGenerator.I.GenerateBlackhole(mouseWorldPosition);
        }
#endif
    }
#endregion
#region interface
    public Vector3 GetRandomStartPosition(out int startDirection, float startPositionOffset)
    {
        startDirection = Helpers.Rand(0, 4);
        return GetBorderPosition(startDirection, startPositionOffset);
    }

    public Vector3 GetRandomTargetPosition(int startDirection)
    {
        var directions = new List<int>() { 0, 1, 2, 3 };
        directions.Remove(startDirection);
        return GetBorderPosition(Helpers.Rand(directions), 0);
    }

    public Vector2 GetBorderPosition(int direction, float positionOffset)
    {
        float randomXPosition = Helpers.Rand(LevelBounds.min.x, LevelBounds.max.x);
        float randomYPosition = Helpers.Rand(LevelBounds.min.y, LevelBounds.max.y);

        float posX = 0;
        float posY = 0;

        if (direction == 0) //up
        {
            posX = randomXPosition;
            posY = LevelBounds.max.y + positionOffset;
        }
        else if (direction == 1) //right 
        {
            posX = LevelBounds.max.x + positionOffset;
            posY = randomYPosition;
        }
        else if (direction == 2) //down
        {
            posX = randomXPosition;
            posY = LevelBounds.min.y - positionOffset;
        }
        else // left
        {
            posX = LevelBounds.min.x - positionOffset;
            posY = randomYPosition;
        }

        return new Vector2(posX, posY);
    }

    public Vector2 GetRandomCenterPosition()
    {
        return new Vector2(LevelBounds.center.x + Helpers.Rand(LevelBounds.min.x, LevelBounds.max.x) * 0.4f, LevelBounds.center.y + Helpers.Rand(LevelBounds.min.x, LevelBounds.max.x) * 0.4f);
    }

    public bool IsInsideLevelBounds(Vector3 position)
    {
        return LevelBounds.Contains(position);
    }

    public bool IsCompletelyOutsideCameraFrustum(Bounds bounds)
    {
        points[0] = new Vector2(bounds.extents.x, bounds.extents.y);
        points[1] = new Vector2(bounds.extents.x, -bounds.extents.y);
        points[2] = new Vector2(-bounds.extents.x, bounds.extents.y);
        points[3] = new Vector2(-bounds.extents.x, -bounds.extents.y);

        if (IsInsideLevelBounds(bounds.center + points[0])) return false;
        if (IsInsideLevelBounds(bounds.center + points[1])) return false;
        if (IsInsideLevelBounds(bounds.center + points[2])) return false;
        if (IsInsideLevelBounds(bounds.center + points[3])) return false;

        return true;
    }
#endregion
#region internal
    private void RestartLevel()
    {
        LensFocusEffect.reducedDelay = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void GotoMenu()
    {
        LensFocusEffect.reducedDelay = false;
        SceneManager.LoadScene("MenuScene");
    }

    private IEnumerator RestartCoroutine()
    {
        yield return new WaitForSeconds(4f);
        RestartLevel();
    }
#endregion
#region events

    private bool Gameover = false;

    private void OnPlayerDeath()
    {
        Gameover = true;
        //StartCoroutine(RestartCoroutine());
        if (OnGameoverEvent != null) OnGameoverEvent();
        ScoreController.I.CheckHighScore();
    }
#endregion
}

