﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreController : MonoBehaviour
{
    public static ScoreController I;
    public Highscores highscores;
    public static float score;
    public float highScore = 0;
    string highScoreKey = "HighScore";
    bool HighscoreSet = false;

    public Text text;
    // Use this for initialization
    void Awake()
    {
        I = this;
        score = 0;
        highScore = PlayerPrefs.GetFloat(highScoreKey, 0);
    }

    // Update is called once per frame
    void Update()
    {
        text.text = score.ToString("0");
    }
    public bool CheckHighScore()
    {
        HighscoreSet = true;
        //WebSocketHandler.I.submitHighscore(score);
        if (score > highScore)
        {
            PlayerPrefs.SetFloat(highScoreKey, score);
            PlayerPrefs.Save();
            //Debug.Log(highScore);
            highscores.DisplayHighScore(score);
        }
        else
        {
            highscores.DisplayHighScore(highScore);
        }
        return false;

    }
    public void AddScore(float Pointvalue)
    {
        if (HighscoreSet == false)
        {
            score += Pointvalue;
            //Debug.Log(score);
        }

    }


}