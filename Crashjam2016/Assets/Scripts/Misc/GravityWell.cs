﻿using UnityEngine;
using System.Collections;

public class GravityWell : MonoBehaviour
{
    public new Rigidbody2D rigidbody;
    public new CircleCollider2D collider;
    public bool distanceAffectsForce = false;

    public float force { get; set; }

    private void Start()
    {
        force = rigidbody.mass;
    }

    public void OnTriggerStay2D(Collider2D collider)
    {
        var targetRigidbody = collider.GetComponent<Rigidbody2D>();
        var direction = (rigidbody.position - targetRigidbody.position).normalized;

        float f = force;
        if (distanceAffectsForce)
        {
            float distance = Vector2.Distance(rigidbody.position, targetRigidbody.position);
            float radius = this.collider.radius * this.collider.transform.lossyScale.x;
            f *= 1f - (distance / radius);
            //Debug.Log("Force "+f);
        }

        targetRigidbody.AddForce(direction * f, ForceMode2D.Force);
    }
}
