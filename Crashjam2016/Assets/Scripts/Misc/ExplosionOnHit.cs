﻿using UnityEngine;
using System.Collections;

public class ExplosionOnHit : MonoBehaviour
{
    public float explosionForce = 10f;
    public float explosionRadius = 10f;

    public bool multiplyByMass = true;
    public bool onHit = true;
    public bool individualHit = false;

    public GameObject explosionEffect;
    public LayerMask layerMask;
    public InteractionSystem interactionSystem;
    public GravityWell gravityWell;

    public void Awake()
    {
        if (onHit)
            interactionSystem.OnCollisionReceived += OnCollisionReceived;
    }

    public void OnCollisionReceived(InteractionSystem interactionSystem)
    {
        if ((layerMask.value & interactionSystem.Mask) == interactionSystem.Mask)
        {
            if (individualHit)
            {
                ApplyForce(interactionSystem);
                if (explosionEffect != null)
                Instantiate(explosionEffect, transform.position, Quaternion.identity);
            }
            else
                PlayExplosion();
        }
    }

    public void PlayExplosion()
    {
        PlayExplosion(transform.position);
    }

    public void PlayExplosion(Vector3 position)
    {
        var colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius, layerMask.value);

        if(explosionEffect != null)
        Instantiate(explosionEffect, transform.position, Quaternion.identity);

        for (int i = 0; i < colliders.Length; i++)
        {
            ApplyForce(colliders[i].GetComponent<InteractionSystem>());
        }
    }

    public void ApplyForce(InteractionSystem target)
    {
        float f = explosionForce;
        if (multiplyByMass)
        {
            float mass = interactionSystem.Rigidbody.mass;

            if (gravityWell != null)//quick hack!
                mass = gravityWell.force;

            f *= mass;
        }

        target.AddExplosiveForce(transform.position, f);
    }
}
