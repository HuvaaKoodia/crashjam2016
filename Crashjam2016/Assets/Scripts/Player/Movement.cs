﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    public float LerpSpeed = 2f;
    public float MinSpeed = 0f, MaxSpeed = 10.0f;
    public float MinDistance = 0.1f, MaxDistance = 5f;
    [Range(0,1f)]
    public float MinRotationSpeedMultiplier = 0.3f;
    public Rigidbody2D Rigidbody;

    void FixedUpdate()
    {
        movement();
    }

    public void movement()
    {
        var mouseScreenPosition = Input.mousePosition;
        mouseScreenPosition.z = - Camera.main.transform.position.z;
        var mouseWorldPosition = Camera.main.ScreenToWorldPoint(mouseScreenPosition);
        mouseWorldPosition.z = 0;

        //linear interpolation
        //var movePosition = Vector3.Lerp(transform.position, mouseWorldPosition, Time.deltaTime * LerpSpeed);
        //Rigidbody.MovePosition(movePosition);

        //linear movement
        var movementDirection = transform.position.ToNorm(mouseWorldPosition);
        float distance = Vector3.Distance(transform.position, mouseWorldPosition);
        //no force if inside min distance
        if (distance < MinDistance) distance = 0;
        float distancePercent = Mathf.Min(distance, MaxDistance) / MaxDistance;
        //speed based on distance (slower if closer)
        float speed = MinSpeed + (MaxSpeed - MinSpeed) * distancePercent;

        //slerp speed based on turning angle (bigger angle -> slower slower rotation, effect lesser when close by)
        float dot = (1f + Vector3.Dot(Rigidbody.velocity.normalized, movementDirection)) / 2f;
        dot = Mathf.Max(MinRotationSpeedMultiplier + (1f - distancePercent) * (1- MinRotationSpeedMultiplier), dot);
        //Debug.Log("DOT: "+dot);

        Rigidbody.velocity = Vector3.Lerp(Rigidbody.velocity, movementDirection * speed, dot * LerpSpeed * Time.fixedDeltaTime);
    }
}

