﻿using UnityEngine;
using System.Collections;

public class DestroyIfOutsideBorders : MonoBehaviour
{
    public float StartDelay = 5f;
    public Rigidbody2D Rigidbody;
    public Collider2D Collider;
    private float timer;
    public InteractionSystem InteractionSystem;

    void Start()
    {
        timer = Time.time + StartDelay;
    }

	void Update ()
    {
        if (Time.time > timer && WorldController.I.IsCompletelyOutsideCameraFrustum(Collider.bounds))//hack timer, but works
        {
            InteractionSystem.Destroy();
        }
	}
}
