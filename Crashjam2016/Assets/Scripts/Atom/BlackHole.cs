﻿using UnityEngine;
using System.Collections;

public class BlackHole : MonoBehaviour
{
    public float LifeTime = 30f;
    public float MassGrowthMultiplier = 2f;
    public float ScaleGrowthMultiplier = 10f;

    public ParticleSystem PS;
    public InteractionSystem InteractionSystem;
    public GrowOverTime GrowOverTime;
    public Rigidbody2D Rigidbody;

    float deathDuration = 4f;

    private void Start ()
    {
        InteractionSystem.OnCollisionReceived += OnCollisionReceived;

        float delay = LifeTime - deathDuration;
        StartCoroutine(LifetimeCoroutine(delay));
        StartCoroutine(IncreaseMassCoroutine(MassGrowthMultiplier, delay));
    }

    private void OnCollisionReceived(InteractionSystem interactionSystem)
    {
        interactionSystem.Die();
    }

    IEnumerator LifetimeCoroutine(float Delay)
    {
        
        GrowOverTime.Grow(Vector3.one * ScaleGrowthMultiplier, Delay, 0);

        yield return new WaitForSeconds(Delay);

        GrowOverTime.Grow(Vector3.zero, deathDuration, 1);

        PS.Stop();
        yield return new WaitForSeconds(5f);
        
        Destroy(gameObject);
    }

    IEnumerator IncreaseMassCoroutine(float targetPercent, float Delay)
    {
        float startTime = Time.time;
        float percent = 0;
        float startMass = Rigidbody.mass;
        while (true)
        {
            percent = 1f + targetPercent * ((Time.time - startTime) / Delay);
            if (percent >= targetPercent) break;

            Rigidbody.mass = startMass * percent;

            yield return null;
        }
    }
}
