﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GrowOverTime : MonoBehaviour 
{
    #region vars
    public AnimationCurve[] Curves;
    private IEnumerator growEnumerator;
    #endregion
    #region init
    public void Grow(Vector3 endScale, float duration, int animationCurveIndex = -1)
    {
        if (growEnumerator != null) StopCoroutine(growEnumerator);

        if (animationCurveIndex != -1)
        {
            growEnumerator = GrowCoroutine(endScale, duration, Curves[animationCurveIndex]);
        }
        else growEnumerator = GrowCoroutine(endScale, duration);
        StartCoroutine(growEnumerator);
    }

	private IEnumerator GrowCoroutine (Vector3 endScale, float duration, AnimationCurve curve = null) 
	{
        float percent = 0;
        float speed = 1 / duration;
        Vector3 startScale = transform.localScale;

        while (true)
        {
            percent += Time.deltaTime * speed;

            if (curve != null)
                transform.localScale = startScale + (endScale - startScale) * curve.Evaluate(percent);
            else 
                transform.localScale = Vector3.Lerp(startScale, endScale, percent);
            if (percent >= 1) break;

            yield return null;
        }
        growEnumerator = null;
	}
#endregion
#region logic
#endregion
#region public interface
#endregion
#region private interface
#endregion
#region events
#endregion
}
