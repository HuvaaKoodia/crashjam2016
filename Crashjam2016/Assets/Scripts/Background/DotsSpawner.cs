﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DotsSpawner : MonoBehaviour {

    public float offSetX = 10.0f, offSetY = 10.0f;
    public GameObject dotsObj;

	// Use this for initialization
	void Start () {
        int i = 0, a = 0;

        while (i < 10)
        {
            while (a < 8)
            {
                Instantiate(dotsObj, new Vector3(transform.position.x + (i * offSetX), transform.position.y + (a * offSetY), 0), Quaternion.identity);
                a++;
            }
            i++;
            a = 0;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
