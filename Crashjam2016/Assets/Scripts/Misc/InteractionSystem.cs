﻿using UnityEngine;
using System.Collections;

public delegate void CollisionEvent(InteractionSystem interactionSystem);

public class InteractionSystem : MonoBehaviour
{
    public Rigidbody2D Rigidbody;
    public bool invincible = false;

    public event CollisionEvent OnCollisionReceived;
    public event System.Action OnExplosionReceivedEvent, OnDeathEvent;
    public event System.Action OnDestroyEvent;

    public int Mask { get; private set; }

    private void Awake()
    {
        Mask = 1 << gameObject.layer;
    }

    public void AddExplosiveForce(Vector2 position, float force)
    {
        Rigidbody.AddForceAtPosition(position.ToNorm(Rigidbody.position) * force, position, ForceMode2D.Impulse);
        if (OnExplosionReceivedEvent != null) OnExplosionReceivedEvent();
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (OnCollisionReceived != null)
        {
            var interactionSystem = collision.gameObject.GetComponent<InteractionSystem>();
            OnCollisionReceived(interactionSystem);
        }
    }

    public void Die()
    {
#if UNITY_EDITOR
        if (invincible) return;
#endif
        if (OnDeathEvent !=null) OnDeathEvent();
    }

   public void Destroy()
    {
        if (OnDestroyEvent != null) OnDestroyEvent();
    }
}
